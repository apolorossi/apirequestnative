package com.example.apolo.apirequestnative;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).run();




    }

    private void getData() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            String value="";
            URL url = new URL("https://api.github.com/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (connection.getResponseCode() == 200) {
                InputStreamReader reader = new InputStreamReader(connection.getInputStream(), "UTF-8");
                JsonReader jsonReader = new JsonReader(reader);

                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    if (jsonReader.nextName().equals("current_user_url")) {
                        value = jsonReader.nextString();
                    } else {
                        jsonReader.skipValue();
                    }

                }
                jsonReader.close();
                connection.disconnect();

                Log.d("API",value.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
